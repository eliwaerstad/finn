package finnTask;

import java.io.*;
import java.util.*;


public class Blackjack {
    /**
     * pull cards from file and shuffle cards
     *
     * @return list of cards randomly shuffle
     */
    public static List<String> ShuffleCards() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/deck.txt"));
            String line = reader.readLine();
            List<String> cards = new ArrayList<>();
            while (line != null) {
                String[] cardsLine = line.split(",");
                Collections.addAll(cards, cardsLine);
                line = reader.readLine();
            }
            // shuffle cards
            Random rand = new Random(System.currentTimeMillis());
            Collections.shuffle(cards, rand);
            return cards;

        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;

    }

    /**
     * deal cards to player and dealer
     */
    public static void initialGame() {
        // first shuffle cards
        List<String> cards = ShuffleCards();
        // was test for one bug //Arrays.asList("H6", "D6", "S3", "S4", "DA", "H10", "CK", "S7", "D9", "C5", "DJ", "C2","C6", "DQ", "C10", "HA", "H7", "S5", "H2", "D5", "C3", "H8", "S10", "C4", "SJ", "S2", "DK", "C9", "HK", "CA", "CJ", "S6", "S8", "CQ", "H9", "C8", "D8", "H3", "HJ", "SA", "D3", "D10", "C7", "HQ", "SK", "S9", "H4", "D7", "D2", "H5", "D4", "SQ");

        List<String> playerHands = new ArrayList<>();
        List<String> dealerHands = new ArrayList<>();
        System.out.println("Shuffled cards : " + cards);

        // Initial: first four cards distribute to player and dealer [player,dealer,player,dealer]

        // player hands
        playerHands.add(cards != null ? cards.get(0) : null);
        playerHands.add(cards != null ? cards.get(2) : null);

        //dealer hands
        dealerHands.add(cards != null ? cards.get(1) : null);
        dealerHands.add(cards != null ? cards.get(3) : null);

        if (cards != null) {
            playGame(playerHands, dealerHands, cards, 4);
        }

    }

    /**
     *  one recursive method
     * @param playerHands first 2 initial cards then after call recursively has player hand
     * @param dealerHands first 2 initial cards then after call recursively has player hand
     * @param cards list of shuffled card
     * @param nextCardIndex index of next available card
     */

    private static void playGame(List<String> playerHands, List<String> dealerHands, List<String> cards, int nextCardIndex) {
        String winner;


        if (isBlackJack(playerHands)) { // if sam is black jack is winner in any aspect
            winner = "Sam";
            showOutput(winner, playerHands, dealerHands);
        } else if (isBlackJack(dealerHands)) { //  dealer is winner if sam is not black jack
            winner = "Dealer";
            showOutput(winner, playerHands, dealerHands);
        } else if (isBust(playerHands)) {
            winner = "Dealer";
            showOutput(winner, playerHands, dealerHands);
        } else if (isBust(dealerHands)) {
            winner = "Sam";
            showOutput(winner, playerHands, dealerHands);
        }


        // neither are blackjack or bust
        else {
            List<String> pRanks = getValueOfCards(playerHands);
            int playersTotal = calculateTotal(pRanks);

            List<String> dRanks = getValueOfCards(dealerHands);
            int dealerTotal = calculateTotal(dRanks);

            // if Player has less than 17 , until more than 17  add cards
            if (playersTotal < 17) {
                playerHands.add(cards.get(nextCardIndex));
                nextCardIndex++; // controlling next available card
                playGame(playerHands, dealerHands, cards, nextCardIndex);
            }
            // after player has more than 17 stop then, until dealer has more than player add cards
            else if (dealerTotal <= playersTotal) {
                dealerHands.add(cards.get(nextCardIndex));
                nextCardIndex++; // controlling next available card
                playGame(playerHands, dealerHands, cards, nextCardIndex);
            } else if (dealerTotal > playersTotal) {
                winner = "Dealer";
                showOutput(winner, playerHands, dealerHands);
            }

        }
    }


    /**
     * check if the dealer or player are blackjack
     *
     * @param cards List of cards for player or dealer
     * @return true or false
     */

    private static boolean isBlackJack(List<String> cards) {
        List<String> result = getValueOfCards(cards);
        int sum = calculateTotal(result);
        return sum == 21;

    }

    /**
     * check if the dealer or player are bust (More then 21)
     *
     * @param cards list of cards
     * @return true or false
     */

    private static boolean isBust(List<String> cards) {
        List<String> result = getValueOfCards(cards);
        int sum = calculateTotal(result);
        return sum > 21;
    }


    /**
     * give the list of  cards in hands and get value of it in integer
     *
     * @param cards list hands
     * @return list of value of cards
     */
    private static List<String> getValueOfCards(List<String> cards) {
        List<String> result = new ArrayList<>();
        for (String card : cards) {
            String rank = card.substring(1);
            result.add(rank);
        }
        return result;
    }

    /**
     * send of list of String value of the card as parameter, convert to int and return total value of the cards
     *
     * @param ranks list of String value of cards
     * @return sum of the cards value
     */
    private static int calculateTotal(List<String> ranks) {
        int sum = 0;
        int value;
        for (String rank : ranks) {
            switch (rank) {
                case "2":
                    value = 2;
                    break;
                case "3":
                    value = 3;
                    break;
                case "4":
                    value = 4;
                    break;
                case "5":
                    value = 5;
                    break;
                case "6":
                    value = 6;
                    break;
                case "7":
                    value = 7;
                    break;
                case "8":
                    value = 8;
                    break;
                case "9":
                    value = 9;
                    break;
                case "A":
                    value = 11;
                    break;
                default:
                    value = 10;
                    break;
            }
            sum += value;
        }
        // System.out.println(sum);
        return sum;
    }

    /**
     * output
     *
     * @param winner      "Sam or Dealer"
     * @param playerHands all cards for sam
     * @param dealerHands all cards for dealer
     */

    private static void showOutput(String winner, List<String> playerHands, List<String> dealerHands) {
        System.out.println("the winner is : " + winner);
        System.out.println("Sam cards : " + playerHands);
        System.out.println("dealer cards : " + dealerHands);

    }


}
